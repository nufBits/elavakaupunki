﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CodePlusXaml
{
    public partial class ImageInScrollView : ContentView
    {
        public PostCreationPage vanhempi; //Vanhempi postcreationpage viewi. Pitäisi tehä bubblaus tapahtumalla tai tarkkailija-mallilla kuvan valinta!
        public ImageInScrollView()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Kuvan sourcenimi. Kuvat löytyvät .Droid projektin resourceista
        /// </summary>
        /// <param name="nimi"></param>
        public void KuvanNimi(string nimi)
        {
            kuva.Source = nimi.ToString();
        }

        /// <summary>
        /// Kun kuva valitaan vipua vääntmällä
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKuvaValittu(object sender, ToggledEventArgs e)
        {
           // string kuvanSource = kuva.Source.ToString(); //TODO miten tämä string läheteteään PostCreationPageen kun tämä tapahtuma eskalootuu? Nyt tehää typerästi
           vanhempi.KuvaValittu(kuva);
        }
    }
}
