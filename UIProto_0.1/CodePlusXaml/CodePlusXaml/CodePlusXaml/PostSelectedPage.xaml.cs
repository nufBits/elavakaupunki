﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CodePlusXaml
{
    public partial class PostSelectedPage : ContentPage
    {
        public CodePlusXamlPage vanhempiSivu;
        public PostSelectedPage()
        {
            InitializeComponent();
            //NaytaPostaus();
        }

        internal void NaytaPostaus(Image kuva, string kuvanKommentti, List<string> kommentit, int id, DateTime aika)
        {
            label_postausaika.Text = aika.ToString();
            image_kuva.Source = kuva.Source;
            label_kuvanKommentti.Text = kuvanKommentti;
            if (kommentit.Count == 0) entry_omakommentti.Text = "Ole ensimmäinen kommentoija";
            LisaaKommentit(kommentit);
        }

        private void LisaaKommentit(List<string> kommentit)
        {
            for (int i = 0; i < kommentit.Count; i++)
            {
                Frame apuFrame = new Frame();
                apuFrame.OutlineColor = Color.Lime;
                apuFrame.BackgroundColor = Color.Navy;
                
                Label kommentti = new Label();
                kommentti.Text = kommentit[i];
                apuFrame.Content = kommentti;

                Stack_kommentit.Children.Add(apuFrame);
                
            }
        }

        /*
        public void NaytaPostaus(Post naytettava) //Inconsistent accessibility: parameter 'Post' is less accessible than method 'PostSelectedPage.NaytaPostaus(Post)'
        {
        //TODO pitäisi selvittää em. ongelman syy. Johtuu jostain Post luokan näkyvyydestä (tai jonku siihen liittyvän jutun näkyvyydestä).
        //em. errorin vuoksi ei voida kuljetella Post-olioita muiden sivujen ja viewien välillä, vaan joudutaan kulettamaan postauksen parametrejä,
        //mikä on erittäin kankeaa. Tämä pitäisi selvittää!
        }
        

        public void NaytaPostaus()
        {
            DisplayAlert("asd","asd","asd");
        }
        */
    }
}
