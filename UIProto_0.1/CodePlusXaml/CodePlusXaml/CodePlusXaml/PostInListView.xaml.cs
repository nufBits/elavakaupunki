﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CodePlusXaml
{
    public partial class PostInListView : ContentView
    {
        public CodePlusXamlPage vanhempiSivu;
        public PostInListView()
        {
            InitializeComponent();
        }

        public void AsetaKuvateksti(string teksti) //TODO bindaamalla nämä arvot ja dpdPropertyjen avulla sitte joskus
        {
            label_kuvateksti.Text = teksti;
        }

        public void AsetaKuva(Image kuva)
        {
            image_postaus.Source = kuva.Source;
        }

        public void AsetaLaelPostausId(int id)
        {
            label_postausId.Text = id.ToString();
        }

        public void AsetaJulkaisuaika(DateTime aika)
        {
            label_postausaika.Text = aika.ToString();
        }

        void OnPostValittu(object sender, EventArgs e)
        {
            int outNum;
            bool onkoNumero = int.TryParse(label_postausId.Text.ToString(), out outNum);
            if (onkoNumero == false) return; //TODO ei tehä vielä mitään jos antaa muita ku numeroita
            int valittuId = outNum;
            vanhempiSivu.NaytaPostaus(valittuId);       
        }
    }
}
