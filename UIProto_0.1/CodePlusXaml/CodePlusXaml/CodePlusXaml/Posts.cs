﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePlusXaml
{
    class Posts   
    {
        private List<Post> p_postaukset = new List<Post>();
        private int postId = 0;

        public List<Post> Postaukset
        {
            get { return p_postaukset; } set { p_postaukset = value; }
        }  

        public void LisaaPostaus(Post post)
        {
            post.id = postId;
            postId++;
            p_postaukset.Add(post);
        }
    }
}
