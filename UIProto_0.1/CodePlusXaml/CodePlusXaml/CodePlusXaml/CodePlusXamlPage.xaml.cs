﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CodePlusXaml
{
    public partial class CodePlusXamlPage : ContentPage
    {
        private List<string> testikommentit = new List<string>();
        private List<Post> postaukset = new List<Post>();
        private int postId = 0;
        Posts posts = new Posts(); //Tätä luokkaa tuskin tarvitaan ekassa protossa
        public CodePlusXamlPage()
        {
            InitializeComponent();
            luoTestiKommentit();
            LuoTestiPostaukset();
            //postaukset = posts.Postaukset;
            LisaaPostauksetNakymaan();
        }


        /// <summary>
        /// Näytetään yksittäinen postaus kun sen kuvaa on scrollista painettu
        /// </summary>
        /// <param name="postauksenId"></param>
        public void NaytaPostaus(int postauksenId)
        {
            //PostCreationPage p = new PostCreationPage();
           // p.vanhempiSivu = this;
           // Navigation.PushAsync(p);
            Post valittu = HaePostaus(postauksenId);
            if (valittu == null) return;
            PostSelectedPage p = new PostSelectedPage();
            p.vanhempiSivu = this;
            p.NaytaPostaus(valittu.kuva, valittu.KuvanKommentti, valittu.kommentit, valittu.id, valittu.julkaisuaika);//TODO tässä sitte postauksen parametrit
            Navigation.PushAsync(p);
           // DisplayAlert("s",valittu.id.ToString(), valittu.KuvanKommentti);

            //TODO pitää tehä oma page ja viedä postaus sinne. Tässä voi tulla ongelma
        }

        private Post HaePostaus(int postauksenId)
        {
            for (int i = 0; i < postaukset.Count; i++)
            { 
                if (postaukset[i].id == postauksenId) return postaukset[i];
            }
            return null; //TODO jonkinlainen poikkeus jos ei löydy
        }


        /// <summary>
        /// Luodaan uusi postaus annetuista parametreista, lisätään se postauslistaan ja luodaan uusi
        /// postausviewi pääikkunan postauscrolliin
        /// </summary>
        /// <param name="kuva"></param>
        /// <param name="kuvateksti"></param>
        public void LisaaPostaus(Image kuva, string kuvateksti) //TODO muutoksia kun lisätään attribuutteja postaukselle
        {
            List<string> listApu = new List<string>();
            Post apu = new Post();
            apu.id = postId; postId++;
            apu.julkaisuaika = DateTime.Now; //postauksen luonti ja postausviewi aliohjelmaan?
            apu.kuva = kuva;
            apu.KuvanKommentti = kuvateksti;
            apu.kommentit = listApu;
            postaukset.Add(apu);
            PostInListView apuView = new PostInListView();
            apuView.vanhempiSivu = this;
            apuView.AsetaLaelPostausId(apu.id);
            apuView.AsetaJulkaisuaika(apu.julkaisuaika);
            apuView.AsetaKuvateksti(apu.KuvanKommentti);
            apuView.AsetaKuva(kuva);
            stack_scroll.Children.Add(apuView);
        }


        void LuoJulkaisuClicked(object sender, EventArgs e)
        {
            PostCreationPage p = new PostCreationPage();
            p.vanhempiSivu = this;
            Navigation.PushAsync(p);
         // DisplayAlert("jamma", "jamma", "cancel");
        }

        /// <summary>
        /// Lisataan testipostaukset päänäkymään scrollin sisään
        /// </summary>
        private void LisaaPostauksetNakymaan() 
        {
            for(int i = 0; i < postaukset.Count; i++)
            {
                //TODO postauksen id frameen kiinni
                PostInListView apu = new PostInListView();
                apu.vanhempiSivu = this;
                apu.AsetaJulkaisuaika(postaukset[i].julkaisuaika);
                apu.AsetaKuvateksti(postaukset[i].KuvanKommentti);
                apu.AsetaLaelPostausId(postaukset[i].id);
                stack_scroll.Children.Add(apu);
            }
        }


        /// <summary>
        /// Luodaan testikommentit
        /// </summary>
        private void luoTestiKommentit()
        {
            for(int i = 0; i < 3; i++)
            {
                testikommentit.Add("Olipa maukasta pullaa");
                testikommentit.Add("Oli ihan paska pulla");
                testikommentit.Add("Vieläkö pullia tarjolla ilmaseksi?");
                testikommentit.Add("Kyllä löytyy. Nopeimmalle kaksi pullaa!");
            }

        }


        /// <summary>
        /// Luodaan samanlaisia testipostauksia kymmenisen kpl
        /// </summary>
        /// <returns></returns>
        private void LuoTestiPostaukset()
        {
            for(int i = 0; i < 1; i++)
            {
                Image kuva = new Image();
                kuva.Source = "jkl";
                Post apuPost = new Post();
                apuPost.id = postId; postId++; //Aina lisättävä myös id:tä kun tehään postaus
                apuPost.KuvanKommentti = "Siisti pulla ilmaseksi";
                apuPost.kommentit = testikommentit;
                apuPost.kuva = kuva;
                apuPost.julkaisuaika = DateTime.Now;
                
                postaukset.Add(apuPost);
                posts.LisaaPostaus(apuPost);
            }

        }
    }

}
