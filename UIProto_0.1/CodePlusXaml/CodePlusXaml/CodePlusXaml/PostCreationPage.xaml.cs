﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CodePlusXaml
{
    public partial class PostCreationPage : ContentPage
    {
        private string genre;
        private Image postKuva;
        public CodePlusXamlPage vanhempiSivu;

        private List<string> kuvienNimet = new List<string>();
        public PostCreationPage()
        {
            InitializeComponent();
            HaeKuvienNimet();
            LisaaKuvavalikko();
        }


        /// <summary>
        /// Lisätään kuvavalikko lomakkeelle
        /// </summary>
        private void LisaaKuvavalikko()
        { 
            for (int i = 0; i < kuvienNimet.Count; i++)
            {
                var apu = new ImageInScrollView();
                apu.vanhempi = this;
                apu.KuvanNimi(kuvienNimet[i]);
                
                stack_imageScroll.Children.Add(apu);
            }
        }

        /// <summary>
        /// Kun kuva valitaan scrollista. Scrolli piilotetaan ja valittu kuva näytetään.
        /// Tulevaisuudessa pitää toimia niin, että voi ottaa kuvan kameralla. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKuvaValittu(object sender, ToggledEventArgs e)
        {
            stack_imageScroll.IsEnabled = false;
            stack_imageScroll.IsVisible = false;
        }


        /// <summary>
        /// Julkaistaan postaus valitulla kuvalla ja kuvatekstillä. Automaattisesti 
        /// annetaan julkaisulle julkaisuajankohta. 
        /// </summary>
        /// <param name="sender">Julkaisupaike</param>
        /// <param name="e">Tapahtumatietoja</param>
        private void OnJulkaiseClicked(object sender, EventArgs e)
        {
            //DisplayAlert(DateTime.Now.ToString(),entry_kuvaus.ToString(),postKuva.Source.ToString());
            Post uusi = new Post();
            uusi.julkaisuaika = DateTime.Now;
            uusi.kuva = postKuva;
            uusi.KuvanKommentti = entry_kuvaus.Text.ToString();
            //vanhempiSivu.PostausParametrit(postKuva, entry_kuvaus.Text.ToString());
            vanhempiSivu.LisaaPostaus(postKuva, entry_kuvaus.Text.ToString());
            Navigation.RemovePage(this);
        }


        /// <summary>
        /// Maailman vammaisin tapa tehä tämä, mutta ei kerkiä selvittämään miten järkevämmin
        /// </summary>
        private void HaeKuvienNimet()
        {
            kuvienNimet.Add("jkl");
            kuvienNimet.Add("kahvila");
            kuvienNimet.Add("konsertti");
            kuvienNimet.Add("parturi");
            kuvienNimet.Add("ravintola");
        }


        /// <summary>
        /// Kun painetaan mitä tahansa switchiä, disabloidaan ja piilotetaan switchit ja näytetään lopulinen
        /// ilmoituksen tekemiseen tarkoitettu lomake
        /// </summary>
        /// <param name="sender">Lähettäjä</param>
        /// <param name="e">Tapahtumatietoja kaiketi</param>
        void OnSwitchToggled (object sender, ToggledEventArgs e) //TODO miten tuodaan ontoggledissa parametrinä genre?
        {
            stack_switchit.IsEnabled = false;
            stack_switchit.IsVisible = false;
            stack_luomislomake.IsEnabled = true;
            stack_luomislomake.IsVisible = true;
            genre = "ilmoitus"; //Nyt siis tehään aina ilmotuksia, riippumatta mitä valitaan
        }

        /// <summary>
        /// Aliohjelma, joka ottaa vastaan kuvan nimen (sourcen), hävittää horisontaalisen
        /// kuvavalikon ja näyttää valitun kuvan jonka nimi parametrina tuodaan.
        /// </summary>
        /// <param name="source">Resursseissa olevan kuvan nimi</param>
        public void KuvaValittu(Image valittuKuva)
        {
            scroll_kuvat.IsEnabled = false;
            scroll_kuvat.IsVisible = false;
            image_valittu.Source = valittuKuva.Source;
            stack_valittu_kuva.IsEnabled = true;
            stack_valittu_kuva.IsVisible = true;
            postKuva = valittuKuva;
        }

    }
}
