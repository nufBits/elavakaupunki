//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CodePlusXaml {
    using System;
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;
    
    
    public partial class PostInListView : ContentView {
        
        private Label label_postausId;
        
        private Label label_postausaika;
        
        private Image image_postaus;
        
        private Label label_kuvateksti;
        
        private void InitializeComponent() {
            this.LoadFromXaml(typeof(PostInListView));
            label_postausId = this.FindByName<Label>("label_postausId");
            label_postausaika = this.FindByName<Label>("label_postausaika");
            image_postaus = this.FindByName<Image>("image_postaus");
            label_kuvateksti = this.FindByName<Label>("label_kuvateksti");
        }
    }
}
