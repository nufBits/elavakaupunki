﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CodePlusXaml
{
    class Post
    {
        private int pId;
        private int pId_julkaisija;
        private int pTykkaykset = 0;
        private string pKuvan_kommentti;
        private string pGenre;
        private Image pKuva;
        private DateTime pJulkaisuaika;
        private List<string> pKommentit;

        public int id
        {
            get { return pId; } set { pId = value; }
        }

        public int id_julkaisija
        {
            get { return pId_julkaisija; }set { pId_julkaisija = value; }
        }

        public int tykkays
        {
            get { return pTykkaykset; } set { pTykkaykset += 1; }
        }

        public List<string> kommentit
        {
            get { return pKommentit; } set { pKommentit = value; }
        }

        public string KuvanKommentti
        {
            get { return pKuvan_kommentti; } set { pKuvan_kommentti = value; }
        }

        public string Genre
        {
            get { return pGenre; } set { pGenre = value; }
        }

        public Image kuva
        {
            get { return pKuva; } set { pKuva = value; }
        }

        public DateTime julkaisuaika
        {
            get { return pJulkaisuaika; } set { pJulkaisuaika = value; } 
        }

        public override string ToString()
        {
            return pId.ToString() + ", " + pId_julkaisija.ToString();
        }

        public static void main()
        {

        }

    }

    
}
